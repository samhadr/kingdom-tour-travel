<?php
/**
 * The template for displaying Home page posts.
 *
 * @package ThinkUpThemes
 */

get_header(); ?>

		<div class="title-banner banner-main">
			<div class="banner-content">
				<p><?php get_current_blog_id() ?></p>
			</div><!-- end banner-content -->
		</div><!-- end title-banner -->

		<div class="container quote-paragraph portfolio-wrapper">
			<div class="row">
				<div class="home-quote column-3">
					<?php dynamic_sidebar( 'Home Quote' ); ?>
				</div><!-- end home-quote column-3 -->
				<div class="home-paragraph column-6">
					<?php dynamic_sidebar( 'Home Paragraph' ); ?>
				</div><!-- end column-6 -->
			</div><!-- end row -->
		</div><!-- end .container -->

		<?php query_posts('category_name=front page'); ?>

			<?php if( have_posts() ): ?>

				<div id="container" class="portfolio-wrapper home">

				<?php while( have_posts() ): the_post(); ?>

					<div class="blog-grid element column-3">

					<article id="post-<?php the_ID(); ?>" <?php post_class('cat-'.get_first_category_ID()); ?>>

						<?php thinkup_input_blogimage(); ?>
						<?php thinkup_input_blogformat(); ?>

						<header class="entry-header">
							<?php think_input_blogtitle(); ?>
							<?php the_meta(); ?>
						</header>

					</article><!-- #post-<?php get_the_ID(); ?> -->	

					</div>

				<?php endwhile; ?>



				</div><div class="clearboth"></div>

				<?php thinkup_input_pagination(); ?>

				

			<?php else: ?>

				<?php get_template_part( 'no-results', 'archive' ); ?>		

			<?php endif; wp_reset_query(); ?>

			<?php /* Custom Slider */ thinkup_input_sliderhome(); ?>

				<div class="container">
					<div class="row">
						<!--<div class="home-image">
							<?php /* dynamic_sidebar( 'Home Image' ); */ ?>
						</div>-->
					</div><!-- end row -->
					<div class="row">
						<div class="home-testimonial column-3">
							<?php dynamic_sidebar( 'Home Testimonial' ); ?>
						</div><!-- end home-quote column-3 -->
						<div class="column-6 pull-right">
							<?php dynamic_sidebar( 'Newsletter' ); ?>
						</div><!-- end column-6 -->
					</div><!-- end row -->
				</div><!-- end .container -->

<?php get_footer() ?>