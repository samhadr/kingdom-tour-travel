<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id="main-core".
 *
 * @package ThinkUpThemes
 */
?>

		</div><!-- #main-core -->
		</div><!-- #main -->
		<?php /* Sidebar */ thinkup_sidebar_html(); ?>
	</div>
	</div><!-- #content -->

	<footer>
		<?php /* Custom Footer Layout */ thinkup_input_footerlayout();
		echo	'<!-- #footer -->';  ?>
		
		<div id="sub-footer">
		<div id="sub-footer-core">	
		
			<?php if ( has_nav_menu( 'sub_footer_menu' ) ) : ?>
			<?php wp_nav_menu( array( 'depth' => 1, 'container_class' => 'sub-footer-links', 'container_id' => 'footer-menu', 'theme_location' => 'sub_footer_menu' ) ); ?>
			<?php endif; ?>
			<!-- #footer-menu -->

			<div class="copyright container">
				<div class="row">
					<div class="column-4">
						P.O. Box 782008 San Antonio, TX 78278
					</div><!-- end column-4 -->
					<div class="column-4">
						Office: (210)-735-7800
					</div><!-- end column-4 -->
					<div class="column-4">
						Fax: (310)-333-0000
					</div><!-- end column-4 -->
					<div class="column-4">
						Email: <a href="/contact">KingTours@gmail.com</a>
					</div><!-- end column-4 -->
				</div><!-- end row -->
			</div>
			<!-- .copyright -->

		</div>
		</div>
	</footer><!-- footer -->

</div><!-- #body-core -->

<?php wp_footer(); ?>

<!--<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>-->

</body>
</html>