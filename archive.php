<?php
/**
 * The template for displaying Archive pages.
 *
 * @package ThinkUpThemes
 */

get_header(); ?>

			<?php if( have_posts() ): ?>

				<div id="container" class="portfolio-wrapper">

				<?php while( have_posts() ): the_post(); ?>

					<div class="blog-grid element column-3">

					<article id="post-<?php the_ID(); ?>" <?php post_class('cat-'.get_first_category_ID()); ?>>

						<?php thinkup_input_blogimage(); ?>
						<?php thinkup_input_blogformat(); ?>

						<header class="entry-header">
							<?php think_input_blogtitle(); ?>
							<?php the_meta(); ?>

						</header>

					</article><!-- #post-<?php get_the_ID(); ?> -->	

					</div>

				<?php endwhile; ?>

				</div><div class="clearboth"></div>

				<?php thinkup_input_pagination(); ?>

			<?php else: ?>

				<?php get_template_part( 'no-results', 'archive' ); ?>		

			<?php endif; wp_reset_query(); ?>

<?php get_footer() ?>