<?php
/**
 * The Single Post content template file.
 *
 * @package ThinkUpThemes
 */
?>

		<?php /* thinkup_input_postmeta(); */ ?>

		<div class="title-banner">
			<div class="banner-content">
				<?php think_input_blogtitle(); ?>
				<?php the_meta(); ?>
			</div><!-- end banner-content -->
		</div><!-- end title-banner -->

		<div class="entry-content">
			<?php the_excerpt(); ?>
			<?php the_content(); ?>
		</div><!-- .entry-content -->