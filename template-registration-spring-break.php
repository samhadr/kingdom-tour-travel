<?php
/**
 * Template Name: Registration Spring Break
 *
 * @package ThinkUpThemes
 */  


get_header(); ?>

			<div class="title-banner spring-break">
				<div class="banner-content">
					<h1><?php echo get_the_title(); ?></h1>
				</div><!-- end column-1 -->
			</div><!-- end title-banner -->

			<div class="container">
				<div class="row">
					<div class="column-6">
						<?php while ( have_posts() ) : the_post(); ?>

							<?php get_template_part( 'content', 'page' ); ?>

							<?php thinkup_input_nav( 'nav-below' ); ?>

							<?php /* thinkup_input_allowcomments(); */ ?>

						<?php endwhile; wp_reset_query(); ?>
					</div><!-- end column-6 -->
					<div class="column-3 pull-right">
						<?php dynamic_sidebar( 'Spring Break Registration Sidebar' ); ?>
					</div><!-- end column-3 -->
					
				</div><!-- end row -->
			</div><!-- end container -->

<?php get_footer(); ?>